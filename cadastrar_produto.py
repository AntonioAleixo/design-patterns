from Cadastro.adapters import ProdutoAdapter
from Cadastro.builders import ProdutoBuilder
from Cadastro.singletons import ProdutoSingleton

def cadastrar_produto():
    produto_singleton = ProdutoSingleton()

    dados_produto = ProdutoBuilder().prod_nome(input('Digite o nome do produto: ')).prod_descricao(input('Digite a descrição do produto: ')).build()

    adapter = ProdutoAdapter()
    adapter.salvar_produto(**dados_produto)

    print('Produto cadastrado com sucesso!')

def exibir_produtos():

    adapter = ProdutoAdapter()
    produtos = adapter.listar_produtos()

    print('Produtos cadastrados:')
    for produto in produtos:
        print(f'ID: {produto[0]}, Nome: {produto[1]}, Descrição: {produto[2]}')

if __name__ == "__main__":
    cadastrar_produto()
    exibir_produtos()
