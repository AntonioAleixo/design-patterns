
class ProdutoBuilder:
    def __init__(prod):
        prod.nome = ""
        prod.descricao = ""

    def prod_nome(prod, nome):
        prod.nome = nome
        return prod

    def prod_descricao(prod, descricao):
        prod.descricao = descricao
        return prod

    def build(prod):
        return {"nome": prod.nome, "descricao": prod.descricao}
