
import sqlite3

class ProdutoAdapter:
    def __init__(save, database_name='db.sqlite3'):
        save.conn = sqlite3.connect(database_name)
        save.cursor = save.conn.cursor()

        save.cursor.execute('''
            CREATE TABLE IF NOT EXISTS produtos (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                nome TEXT,
                descricao TEXT
            )
        ''')
        save.conn.commit()

    def salvar_produto(save, nome, descricao):
        save.cursor.execute('INSERT INTO produtos (nome, descricao) VALUES (?, ?)', (nome, descricao))
        save.conn.commit()

    def listar_produtos(save):
        save.cursor.execute('SELECT * FROM produtos')
        return save.cursor.fetchall()
