
class ProdutoSingleton:
    _instance = None

    def __new__(cls):
        if not cls._instance:
            cls._instance = super(ProdutoSingleton, cls).__new__(cls)
            cls._instance.nome = "Produto Padrão"
            cls._instance.descricao = "Descrição Padrão"
        return cls._instance
